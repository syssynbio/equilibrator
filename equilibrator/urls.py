"""equilibrator URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView, TemplateView

from gibbs import views

urlpatterns = [
    path('admin/', admin.site.urls),
#    path('gibbs/', include('gibbs.urls')),
    path('', TemplateView.as_view(template_name='main.html'), name='index'),
    path(r'compound_image', views.CompoundImage),
    path(r'compound', RedirectView.as_view(url='/static/compound.html')),
    path(r'metabolite', views.CompoundPage),
    path(r'download', views.DownloadPage),
    path(r'api', RedirectView.as_view(url='https://gitlab.com/elad.noor/equilibrator-api/tree/master')),
    path(r'enzyme', views.EnzymePage),
    path(r'reaction', views.ReactionPage),
    path(r'graph_reaction', views.ReactionGraph),
    path(r'data_refs', views.RefsPage),
    path(r'search', views.ResultsPage),
    path(r'suggest', views.SuggestJson),
    path('pathway/', include('pathway.urls')),
    path(r'robots\.txt', TemplateView.as_view(template_name='robots.txt')),
    path(r'404', views.handler404),
    path(r'500', views.handler500),
    path(r'504', views.handler504),
    path(r'timeout', TemplateView.as_view(template_name='pathway_timeout.html'), name='timeout'),
] 

handler404 = views.handler404
handler500 = views.handler500
handler504 = views.handler504
