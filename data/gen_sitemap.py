#!/usr/bin/python

BASE_URL = 'http://equilibrator.weizmann.ac.il'
LINK_FORMAT = BASE_URL + '%s\n'

def main():
    urls = ['', 'download', 'data_refs', 'pathway',
            'static/classic_rxns/']
    
    urls = map(lambda l: BASE_URL + '/' + l + '\n', urls)
    
    f = open('static/sitemap.txt', 'w')
    f.writelines(urls)
    f.close()
            

if __name__ == '__main__':
    main()
