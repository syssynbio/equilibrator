from django.contrib import admin
from django.urls import path

from . import views

admin.autodiscover()

urlpatterns = [
    path(r'', views.DefinePathwayPage, name='index'),
    path(r'build_model', views.BuildPathwayModel),
    path(r'results', views.PathwayResultPage),
]
