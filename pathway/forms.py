from django import forms

ANALYSIS_CHOICES = {'MDF': 'Max-min Driving Force',
                    'ECM': 'Enzyme Cost Minimization'}

class BuildPathwayModelForm(forms.Form):
    pathway_file = forms.FileField(required=True)
    min_c = forms.FloatField(required=False)
    max_c = forms.FloatField(required=False)
    pH = forms.FloatField(required=False)
    ionic_strength = forms.FloatField(required=False)
    conc_units = forms.CharField(required=True)
    mdf_ecm_toggle = forms.BooleanField(required=False)
    
    def GetOptimizationMethod(self):
        if self.cleaned_data['mdf_ecm_toggle']:
            return 'ECM'
        else:
            return 'MDF'

class AnalyzePathwayModelForm(forms.Form):
    pathway_file = forms.FileField(required=True)
    html_sbtab_toggle = forms.BooleanField(required=False)

    def GetOutputFormat(self):
        if self.cleaned_data['html_sbtab_toggle']:
            return 'SBtab'
        else:
            return 'HTML'
