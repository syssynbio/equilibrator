import logging
from io import StringIO

import numpy as np
import pandas as pd
import seaborn as sns
from ecm import ECMmodel
from matplotlib import pyplot as plt
from numpy import array, cumprod, diff, exp, hstack, ones
from sbtab.SBtab import SBtabTable

from util import constants

from . import (ParsedPathway, PathwayAnalysisData, PathwayAnalyzer,
               PathwayParseError)


class EnzymeCostMinimization(PathwayAnalyzer):
    """
        A class for performing Enzyme Cost Minimization analysis on a given
        pathway (see https://doi.org/10.1371/journal.pcbi.1005167)
    """

    def __init__(self, parsed_pathway, ecm_model):
        super(EnzymeCostMinimization, self).__init__(parsed_pathway)
        self.ecm_model = ecm_model

    def analyze(self):
        lnC = self.ecm_model.ECM()
        if lnC is None:
            raise ValueError('Could not solve this ECM problem')
        return PathwayECMData(self._parsed_pathway, self.ecm_model, lnC=lnC)
    
    @classmethod
    def validate_sbtab(cls, sbtabdoc):
        super(EnzymeCostMinimization, cls).validate_sbtab(sbtabdoc)
        param_sbtab = sbtabdoc.get_sbtab_by_id('Parameter')
        if param_sbtab is None:
            raise PathwayParseError('SBtab does not contain a table names "Parameter"')
        param_df = param_sbtab.to_data_frame()
        qtypes = param_df['QuantityType'].unique()
        if 'Michaelis constant' not in qtypes:
            raise PathwayParseError('No Michaelis constants, cannot run ECM')

    @classmethod
    def from_sbtab(cls, sbtabdoc):
        """Returns an initialized ParsedPathway."""
        parsed_pathway = ParsedPathway.from_sbtab(sbtabdoc)
        
        df_dict = {s.table_id: s.to_data_frame() for s in sbtabdoc.sbtabs}
        
        bound_unit = sbtabdoc.get_sbtab_by_id(
            'ConcentrationConstraint').get_attribute('Unit')
        flux_unit = sbtabdoc.get_sbtab_by_id(
            'RelativeFlux').get_attribute('Unit')
        
        try:
            ecm_model = ECMmodel(df_dict,
                        bound_unit=bound_unit,
                        flux_unit=flux_unit)
        except Exception as e:
            logging.error(str(e))
            raise PathwayParseError('Failed to load the ECM model')
            
        # TODO: write a much more detailed model validation function which
        # raises clear exceptions if some data or table is missing!
        
        return EnzymeCostMinimization(parsed_pathway, ecm_model)

    @classmethod
    def from_csv(cls, fp, bounds=None, aq_params=None):
        """Returns an EnzymeCostMinimization from an input file.

        Caller responsible for closing f.

        Args:
            f: file-like object containing CSV data describing the pathway.
        """
        parsed_pathway = ParsedPathway.from_csv(fp, bounds, aq_params)
        ecm = None
        return EnzymeCostMinimization(parsed_pathway, ecm)

    def to_sbtab(self):
        sbtabdoc = self._parsed_pathway.to_sbtab()
        
        # equilibrium constants        
        param_cols = ['QuantityType', 'Reaction', 'Compound',
                      'Value', 'Unit',
                      'Reaction:Identifiers:kegg.reaction', 'Compound:Identifiers:kegg.compound',
                      'ID']
        param_data = [('equilibrium constant', rxn_id, None,
                       exp(-rxn.dg0_r_prime / constants.RT), 'dimensionless',
                       rxn.stored_reaction_id, None,
                       'kEQ_%s' % rxn_id)
                      for rxn_id, rxn in zip(self.reaction_ids, self.reactions)]

        # add default values for the 3 types catalytic rate constants
        qtype_list = [('catalytic rate constant geometric mean', 'kC'),
                      ('substrate catalytic rate constant', 'kcrf'),
                      ('product catalytic rate constant', 'kcrr')]
        
        for qtype, id_prefix in qtype_list:
            param_data += [(qtype, rxn_id, None,
                            100.0, '1/s',
                            rxn.stored_reaction_id, None,
                            '%s_%s' % (id_prefix, rxn_id))
                           for rxn_id, rxn in zip(self.reaction_ids, self.reactions)]

        for rxn_id, rxn in zip(self.reaction_ids, self.reactions):
            param_data += [('Michaelis constant', rxn_id, c.compound.name_slug,
                            1.0, 'mM',
                            rxn.stored_reaction_id, c.compound.kegg_id,
                            'kM_%s_%s' % (rxn_id, c.compound.name_slug))
                           for c in (rxn.substrates + rxn.products)]
        
        # protein molecular mass    
        param_data += [('protein molecular mass', rxn_id, None,
                        10000, 'Da',
                        rxn.stored_reaction_id, None,
                        'MW_%s' % rxn_id)
                       for rxn_id, rxn in zip(self.reaction_ids, self.reactions)]
        
        # compound moleulcar mass
        param_data += [('molecular mass', None, cpd.name_slug,
                        '%.1f' % cpd.mass, 'Da',
                        None, cpd.kegg_id,
                        'MW_%s' % cpd.name_slug)
                       for cpd in self.compounds]
            
        param_df = pd.DataFrame(columns=param_cols, data=param_data)
        param_sbtab = SBtabTable.from_data_frame(param_df,
                                                 table_id='Parameter',
                                                 table_type='Quantity')
        param_sbtab.change_attribute('pH', '%.2f' % self.aq_params.pH)
        param_sbtab.change_attribute('IonicStrength',
                                     '%.2f' % self.aq_params.ionic_strength)
        param_sbtab.change_attribute('IonicStrengthUnit', 'M')
        sbtabdoc.add_sbtab(param_sbtab)
            
        return sbtabdoc

class PathwayECMData(PathwayAnalysisData):
    
    def __init__(self, parsed_pathway, ecm_model, lnC):
        super(PathwayECMData, self).__init__(parsed_pathway)
        self.ecf = ecm_model.ecf
        self.lnC = lnC

        for rxn, c in zip(self.reaction_data, self.enzyme_costs):
            rxn.enz_conc = c
        
        enz_cost_brkdwn = self.enzyme_costs_breakdown
        for i, rxn in enumerate(self.reaction_data):
            rxn.min_enz_conc = enz_cost_brkdwn[i, 0]
            rxn.eta_th = np.round(1.0/enz_cost_brkdwn[i, 1], 4)
            rxn.eta_sat = np.round(1.0/enz_cost_brkdwn[i, 2], 4)

        cid2conc = dict(zip(ecm_model.kegg_model.cids, exp(self.lnC)))
        for i, cpd in enumerate(self.compound_data):
            cpd.concentration = cid2conc.get(cpd.compound.kegg_id, 1)
        
    @property
    def score(self):
        """
            Return the total enzyme cost in uM
            (convert from M by multiplying by 10^6)
        """
        return self.enzyme_costs.sum() * 1e6
    
    @property
    def enzyme_costs(self):
        return self.ecf.ECF(self.lnC)

    @property
    def enzyme_costs_breakdown(self):
        return array(self.ecf.GetEnzymeCostPartitions(self.lnC))
    
    @property
    def is_ecm(self):
        return True

    @property
    def reaction_plot_svg(self):
        """
            Produces a horizontal stacked bar plot of the enzyme demands, broken down
            into 3 parts:
                - ideal demand (i.e. the minimum amount derived from the flux and kcat)
                - inverse of thermodynamic efficiency (1 / eta^th)
                - inverse of saturation efficiency (1 / eta^sat)
            The values are shown in log-scale, since the total demand is the product
            of these 3 values.
        """
        with sns.axes_style('darkgrid'):
            fig, ax = plt.subplots(figsize=(8, 8))
            ax.grid(color='grey', linestyle='--', linewidth=1, alpha=0.2)
            
            labels = self.ecf.ECF_LEVEL_NAMES[0:3]
            costs = self.enzyme_costs_breakdown[::-1, :] # reverse order of reactions
            
            base = min(filter(None, costs[:, 0])) / 2.0
            idx_zero = (costs[:, 0] == 0)
            costs[idx_zero, 0] = base
            costs[idx_zero, 1:] = 1.0
    
            lefts = hstack([ones((costs.shape[0], 1)) * base,
                            cumprod(costs, 1)])
            steps = diff(lefts)
    
            ind = range(costs.shape[0])    # the x locations for the groups
            height = 0.8
            ax.set_xscale('log')
    
            colors = sns.color_palette('muted', n_colors=6)[3:6]
    
            for i, label in enumerate(labels):
                ax.barh(ind, width=steps[:, i].flat, height=height,
                        left=lefts[:, i].flat, color=colors[i])
    
            ax.set_yticks(ind)
            ax.set_yticklabels(reversed(self.reaction_names), size='medium')
            ax.legend(labels, loc='best', framealpha=0.5)
            ax.set_xlabel('enzyme demand [M]')
            ax.set_xlim(base, None)
            fig.tight_layout()
            
            svg_data = StringIO()
            fig.savefig(svg_data, format='svg')
            return svg_data.getvalue()
