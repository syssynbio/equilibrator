import io
import logging

from django.http import HttpResponse, HttpResponsePermanentRedirect
from django.shortcuts import render
from django.template.context_processors import csrf
from MySQLdb import ProgrammingError
from sbtab.SBtab import SBtabDocument, SBtabError

import stopit
from equilibrator import settings
from util import constants

from . import (EnzymeCostMinimization, MaxMinDrivingForce, PathwayParseError,
               pathway_result_page)
from .forms import AnalyzePathwayModelForm, BuildPathwayModelForm


def DefinePathwayPage(request):
    """Renders the landing page."""
    template_data = csrf(request)
    
    template_data['ionic_strength'] = \
        request.COOKIES.get('ionic_strength', constants.DEFAULT_IONIC_STRENGTH)
    template_data['ph'] = \
        request.COOKIES.get('pH', constants.DEFAULT_PH)

    response = render(request, 'define_pathway_page.html', template_data)
    return response


def BuildPathwayModel(request):
    """Renders a page for a particular reaction."""
    try:
        form = BuildPathwayModelForm(request.POST, request.FILES)

        if not form.is_valid():
            raise Exception(str(form.errors))

        fp = request.FILES['pathway_file']

        try:
            thread_timeout = settings.PATHWAYS['csv_convert_thread_timeout']
            logging.error('Timeout in %d seconds' % thread_timeout)
            with stopit.ThreadingTimeout(thread_timeout) as to_ctx_mgr:
                assert to_ctx_mgr.state == to_ctx_mgr.EXECUTING
                pp, output_fname = pathway_result_page.from_csv(form, fp)
                logging.info(output_fname)

        # sometimes when the time runs out in the middle of an SQL query, we
        # get this exception. The resopnse should be the same as if the
        # to_ctx_mgr indicates a timeout
        except ProgrammingError:
            return render(request, 'pathway_timeout.html', {})
            
        # There was a timeout event, i.e. it took too much time to parse
        # the reactions in the CSV file
        if not to_ctx_mgr:
            return render(request, 'pathway_timeout.html', {})

        response = HttpResponse(content_type='text/tab-separated-values')
        response['Content-Disposition'] = 'attachment; filename="%s"' % \
            output_fname
        response.write(pp.to_sbtab().to_str())
        
        logging.info('build pathway pH = %.2f' % pp.aq_params.pH)
        response.set_cookie('pH', str(pp.aq_params.pH))
        response.set_cookie('ionic_strength', str(pp.aq_params.ionic_strength))
        return response

    
    except Exception as e:
        raise e
        logging.error(e)
        template_data = {'pathway': None,
                         'mdf_result': None,
                         'error_message': str(e)}
        return render(request, 'pathway_result_page.html', template_data)


def PathwayResultPage(request):
    """Renders a page for a particular reaction."""
    
    pp = None
    
    try:
        form = AnalyzePathwayModelForm(request.POST, request.FILES)
        
        if 'pathway_file' not in request.FILES:
            raise ValueError('No file selected.')
        
        if not form.is_valid():
            raise ValueError('Invalid pathway form.')
        
        input_fname = str(request.FILES['pathway_file'])
        if input_fname[-4:] == '.tsv':
            output_fname = input_fname[:-4] + '_report.tsv'
        else:
            output_fname = input_fname + '_report.tsv'
        
        f_data = str(request.FILES['pathway_file'].read(), encoding="utf-8")
            
        thread_timeout = settings.PATHWAYS['pathway_analysis_thread_timeout']
        with stopit.ThreadingTimeout(thread_timeout) as to_ctx_mgr:
            assert to_ctx_mgr.state == to_ctx_mgr.EXECUTING
            sio = io.StringIO(f_data, newline=None)  # universal newline mode
            try:
                sbtab = SBtabDocument('pathway', sio.getvalue(), input_fname)
            except SBtabError as e:
                raise ValueError(str(e))
            
            optimization_method = None
            try:
                EnzymeCostMinimization.validate_sbtab(sbtab)
                pp = EnzymeCostMinimization.from_sbtab(sbtab)
                optimization_method = "ECM"
            except PathwayParseError as e:
                logging.debug(str(e))
            
            if optimization_method is None:
                try:
                    MaxMinDrivingForce.validate_sbtab(sbtab)
                    pp = MaxMinDrivingForce.from_sbtab(sbtab)
                    optimization_method = "MDF"
                except PathwayParseError as e:
                    logging.debug(str(e))
        
            if optimization_method is None:
                raise ValueError('Input SBtab file does not qualify as MDF nor ECM model')
            
            logging.info('Parsed pathway: %s' % optimization_method)
            
            if not pp.validate_dGs():
                raise ValueError('Supplied reaction dG values are inconsistent '
                                 'with the stoichiometric matrix.')
        
            if pp.is_empty():
                raise ValueError('Empty pathway')
        
            # calculate the MDF/ECM with the specified bounds. Render template.
            logging.info('Strating pathway analysis')
            path_data = pp.analyze()
            logging.info('Finished pathway analysis')
        
        if not to_ctx_mgr:
            return HttpResponsePermanentRedirect('/timeout')
        
        logging.info('Calculated %s score: %s' %
                     (optimization_method, path_data.score))
        
        if form.GetOutputFormat() == 'SBtab':
            response = HttpResponse(content_type='text/tab-separated-values')
            response['Content-Disposition'] = 'attachment; filename="%s"' % output_fname
            response.write(path_data.to_sbtab().to_str())
        elif form.GetOutputFormat() == 'HTML':
            template_data = {'pathway': pp,
                             'path_data': path_data,
                             'optimization_method': optimization_method}
            response = render(request, 'pathway_result_page.html', template_data)
        else:
            raise ValueError('unknown output format: ' + form.GetOutputFormat())

        return response
    
    except ValueError as e:
        logging.error(e)
        template_data = {'pathway': pp,
                         'path_data': None,
                         'optimization_method': None,
                         'error_message': str(e)}
        return render(request, 'pathway_result_page.html', template_data)
