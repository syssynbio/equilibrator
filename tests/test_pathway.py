#!/usr/bin/python3
import logging
import os
import re
import unittest
import warnings

import django
from django.test import Client
from sbtab import SBtab

import equilibrator.settings as S
from gibbs.conditions import AqueousParams
from pathway import EnzymeCostMinimization, MaxMinDrivingForce, ParsedPathway
from pathway.bounds import Bounds
from pathway.concs import ConcentrationConverter, NoSuchUnits

COFACTORS_FNAME = os.path.join(S.BASE_DIR, 'pathway/data/cofactors.csv')

class PathwayTester(unittest.TestCase):
    
    def __init__(self, *args, **kwargs):
        super(PathwayTester, self).__init__(*args, **kwargs)
        logging.getLogger().setLevel(logging.WARNING)
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "equilibrator.settings")
        django.setup()
        self.csv_fname = os.path.join(S.BASE_DIR, 'tests',
            'example_pathway_ethanol_fermentation.csv')
        self.mdf_sbtab_fname = os.path.join(S.BASE_DIR, 'tests',
            'example_pathway_ethanol_fermentation_pH7.00_I0.10_MDF.tsv')
        self.ecm_sbtab_fname = os.path.join(S.BASE_DIR, 'tests',
            'example_pathway_ethanol_fermentation_pH7.00_I0.10_ECM.tsv')

        self.client = Client()

    def test_mdf_from_csv_file(self):
        aq_params = AqueousParams(pH=7.0, ionic_strength=0.1)
        bounds = Bounds.from_csv_filename(
            COFACTORS_FNAME, default_lb=1e-6, default_ub=1e-2)

        with open(self.csv_fname, 'r') as fp:
            path = MaxMinDrivingForce.from_csv(fp, bounds=bounds,
                                               aq_params=aq_params)
        
        mdf_res = path.analyze()
        self.assertAlmostEqual(mdf_res.score, 1.69, 1)

    def test_mdf_from_sbtab_file(self):
        sbtabdoc = SBtab.read_csv(self.mdf_sbtab_fname, 'mdf')
            
        self.assertSetEqual(set(ParsedPathway.EXPECTED_TNAMES),
                            set([s.table_id for s in sbtabdoc.sbtabs]))
        
        bs = Bounds.from_sbtab(
            sbtabdoc.get_sbtab_by_id('ConcentrationConstraint'))
        for key in bs.lower_bounds:
            lb = bs.GetLowerBound(key)
            ub = bs.GetUpperBound(key)
            msg = 'bounds for %s lb = %.2g, ub = %.2g' % (key, lb, ub)
            self.assertLessEqual(lb, ub, msg=msg)

        path = MaxMinDrivingForce.from_sbtab(sbtabdoc)
        mdf_res = path.analyze()
    
        self.assertAlmostEqual(mdf_res.score, 1.69, 1)
        
    def test_ecm_from_sbtab_file(self):
        sbtabdoc = SBtab.read_csv(self.ecm_sbtab_fname, 'ecm')
        path = EnzymeCostMinimization.from_sbtab(sbtabdoc)
        ecm_res = path.analyze()
        self.assertAlmostEqual(ecm_res.score, 15142.5, 0)

    def test_unit_string(self):
        test_data = [(1.0, 'M', 1.0),
                     (1.0, 'mM', 1e-3),
                     (10, 'mM', 0.01),
                     (100, 'mM', 0.1),
                     (1.0, 'uM', 1e-6),
                     (150, 'millimolar', 0.15),
                     (13.25, 'micromolar', 13.25e-6)]
        for val, unitstring, expected_out in test_data:
            out = ConcentrationConverter.to_molar_string(val, unitstring)
            self.assertEqual(expected_out, out)

    def test_unit_string_failure(self):
        test_data = [(1.0, 'MOLER'),
                     (1.0, 'milimolar'),
                     (1.0, 'kM'),
                     (150, 'kilimoler'),
                     (13.25, 'macromolar')]
        for val, unitstring in test_data:
            self.assertRaises(
                NoSuchUnits,
                ConcentrationConverter.to_molar_string,
                val, unitstring)

    def test_units(self):
        test_data = [(1.0, ConcentrationConverter.UNITS_M, 1.0),
                     (1.0, ConcentrationConverter.UNITS_MM, 1e-3),
                     (1.0, ConcentrationConverter.UNITS_uM, 1e-6),
                     (150, ConcentrationConverter.UNITS_MM, 0.15),
                     (13.25, ConcentrationConverter.UNITS_uM, 13.25e-6)]
        for val, units, expected_out in test_data:
            out = ConcentrationConverter.to_molar_units(val, units)
            self.assertEqual(expected_out, out)

    def test_web_server(self):
        with open(self.mdf_sbtab_fname, 'r') as fp:
            response = self.client.post('/pathway/results',
                                        {'pathway_file': fp,
                                         'optimization_method': 'MDF'})
        self.assertEqual(response.status_code, 200)
        
        # check that the MDF value is correct
        match = re.findall(r'MDF</a></strong></td>[^<]+<td>([0-9\.]+) kJ/mol</td>',
                           str(response.content))
        for m in match:
            self.assertAlmostEqual(float(m), 1.69, 1)

    def test_sbtab_output(self):
        sbtabdoc = SBtab.read_csv(self.mdf_sbtab_fname, 'mdf')
            
        pp = MaxMinDrivingForce.from_sbtab(sbtabdoc)
        sbtabdoc_out = pp.analyze().to_sbtab()
        
        met_sbtab = sbtabdoc_out.get_sbtab_by_id('PredictedCompoundData')
        enz_sbtab = sbtabdoc_out.get_sbtab_by_id('PredictedReactionData')
        
        self.assertIsNotNone(met_sbtab)
        self.assertIsNotNone(enz_sbtab)
        
        met_df = met_sbtab.to_data_frame().set_index('Compound')
        enz_df = enz_sbtab.to_data_frame().set_index('Reaction')
        
        concs = met_df['Concentration'].apply(float)
        forces = enz_df['Driving force'].apply(float)
        
        self.assertAlmostEqual(concs['3-Phospho-D-glyceroyl phosphate'], 1e-6, 5)
        self.assertAlmostEqual(forces['R_fructose_b_R01070'], 1.69, 2)
