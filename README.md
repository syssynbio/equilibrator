eQuilibrator
============
[![pipeline status](https://gitlab.com/elad.noor/equilibrator/badges/master/pipeline.svg)](https://gitlab.com/elad.noor/equilibrator/commits/master)

eQuilibrator is a biochemical thermodynamics calculator website.
The website enables calculation of reaction and compound energies
through an intuitive free-text interface. The current online
version of eQuilibrator can be found at:
http://equilibrator.weizmann.ac.il/

If you are looking for the back-end library used to estimate standard Gibbs free energies
[DOI: 10.1371/journal.pcbi.1003098](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003098),
you can find it in the following GitLab repository:
[eQuilibrator API](https://gitlab.com/elad.noor/equilibrator-api)

eQuilibrator is written in Python using the Django framework.
It was developed primarily on Ubuntu 18.04.1 LTS and is easiest
to develop and set up on that operating system. Setup instructions
below are really only for Ubuntu users.

# Ubuntu dependencies (apt):
- python 3.5+
- mysql-server (5.7.20)
- libmysqlclient-dev (5.7.20)
- solr-tomcat (3.6.2)
- molconvert (18.12.0), by [ChemAxon](https://chemaxon.com/), must be in PATH

# Python PyPI dependencies (pip):
- Django
- django-extensions
- django-haystack
- matplotlib
- mysqlclient
- nltk
- numpy
- pandas
- pyparsing
- scipy
- pysolr
- seaborn
- sbtab
- enzyme-cost-minimization
- equilibrator-api
- stopit

# Install Ubuntu dependencies
```
sudo apt install mysql-server libmysqlclient-dev python3-pip python3-dev solr-tomcat
```

# Install Python dependencies
```
sudo pip3 install -r requirements.txt
```

# Configure Solr
```
sudo cp solr/schema.xml /etc/solr/conf/
sudo /etc/init.d/tomcat8 restart
```
* Make sure solr is running by going to http://127.0.0.1:8080/solr/

# Create MySQL database and synchronize the data
```
sudo python3 init_db.py
```

# Running the Development Server on a Remote Host

```
sudo python3 manage.py runserver 0.0.0.0:8000
```

Will run the development server on port 8000 and allow external IPs to access
the server. This is very useful for debugging differences between your local
and remote environments.

# Setting up an eQuilibrator web server (using nginx, gunicorn, and Django)

For running eQuilibrator on a web server that can be accessed safely from the
internet, first clone the repository (in the server home directory):
```
cd ~
git clone https://gitlab.com/elad.noor/equilibrator.git
```

Then, follow all the instructions above for running eQuilibrator locally
(i.e. Ubuntu and PyPI dependencies, Solr and MySQL configurations).

Open the file equilibrator/settings.py for editing:
* add the IP of the server to the ALLOWED_HOSTS
* change DEBUG to False

Finally, run these lines from the command line:
```
sudo apt install nginx links gunicorn3
cd ~/equilibrator
sudo python3 manage.py collectstatic

sudo cp config/gunicorn.service /etc/systemd/system/gunicorn.service
sudo systemctl enable gunicorn.service
sudo service gunicorn restart

sudo cp config/nginx.conf /etc/nginx/sites-available/default
sudo cp config/proxy_params /etc/nginx/proxy_params
sudo systemctl enable nginx.service
sudo service nginx restart
```
