#!/usr/bin/python3
import datetime
import logging
import os
import time

import django
from numpy import floor

from equilibrator import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "equilibrator.settings")

def main():
    django.setup()

    logging.info('> Dumping MySQL database into sqldump file')
    db_user, db_name, db_pass = map(settings.DATABASES['default'].get, ['USER', 'NAME', 'PASSWORD'])
    os.environ['MYSQL_PWD'] = db_pass
    cmd = "mysqldump -u %s %s | gzip -c > data/sqldump.txt.gz" % (db_user, db_name)
    os.system(cmd)

if __name__ == '__main__':
    logging.info('Welcome to the sql_load script')

    start = time.time()
    main()
    end = time.time()
    elapsed = datetime.timedelta(seconds=floor(end - start))
    logging.info('Elapsed loading time = %s' % str(elapsed))
